package fr.dauphine.enginethread;

public class SimulationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SimulationException(String message) {
		super(message);
	}

}
