package fr.dauphine.enginethread;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import javax.swing.JOptionPane;

import fr.dauphine.executer.ExecutionManager;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Constants.EXECUTION_STATE;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;
import fr.dauphine.ws.RegisteredService;
import fr.dauphine.ws.Registry;
import fr.dauphine.wscn.MainFrame;
import fr.dauphine.wscn.ShowOutputsDialog;

public class EngineThread extends Thread {
	
	final Lock lock = new ReentrantLock();
	final Condition notFull  = lock.newCondition();
	
	MainFrame mainFrame;
	Class<? extends RegisteredService> servClass;
	Map<Data, String> inputs;
	Map<Data, String> outputs;
	Service service;
	boolean forwardRecovery = true;
	boolean toCompensate = false;
	private boolean stopWaiting;
	private boolean checkpointed = false;
	
	static ShowOutputsDialog showOutputsDialog;

	private ExecutionManager manager;
	
    public EngineThread(MainFrame mainFrame, Service s) {
    	System.out.println("New Thread "  + s.getName());
    	
    	manager = mainFrame.getExecutionManager();
    	
    	forwardRecovery = mainFrame.getExecutionProperties().isForwardRecoveryEnabled();

		this.mainFrame = mainFrame;
		if(!s.equals(Constants.FINAL_NODE.toString()))
			this.servClass = Registry.getServiceClass(s.getName());
		this.service = s;
		this.inputs = new HashMap<Data, String>();
		this.outputs = new HashMap<Data, String>();
		setName(s.getName());
		changeState(EXECUTION_STATE.INITIAL);
	}

    
	public void run() {

        waitForInputs();
        
        checkpointed = inputs.containsValue(ExecuterConstants.Messages.CHECKPOINT_TOKEN.toString());
        
        if(checkpointed) {
        	outputs.clear();
			
			outputs.put(new Data("executer_message"),ExecuterConstants.Messages.CHECKPOINT_TOKEN.toString());
			System.out.println(getName() + " sends " + ExecuterConstants.Messages.CHECKPOINT_TOKEN );
			sendOutputs();
			checkpointed = true;
        } else if(toCompensate) {
        	compensate();
        } else if(!this.service.isControl()) {
	        mainFrame.getLogFrame().publish(new LogRecord(Level.INFO, "Going to execute " + getName() + "\n"));
	        changeState(EXECUTION_STATE.RUNNING);
	        
	        boolean success = false;
	        
	        boolean cantry = false;
	        
	        do {
		        try {
		        	if(!producedAllOutputs()) {
						Thread.sleep((long) 5000);
						doSimulation();
						
						RegisteredService servInstance = servClass.newInstance();
						List<Object> inputs = new ArrayList<Object>();
						inputs.add(this.inputs.get(new Data("text")));
						System.out.println(inputs);
						servInstance.setInputs(inputs);
						servInstance.execute();
						
						
						
						success = true;
						
						changeState(EXECUTION_STATE.EXECUTED);
					
						outputs.put(new Data("text"), (String) servInstance.getOutputs().get(0));
		        	} else
		        		mainFrame.getLogFrame().publish(new LogRecord(Level.INFO, "Skipping " + getName() + "\n"));
					sendOutputs();
				} catch (Exception e) {
					
					//e.printStackTrace();
					mainFrame.getLogFrame().publish(new LogRecord(Level.SEVERE, "There was a problem executing " + getName() + "\n"));
					if(service.isRetriable() && forwardRecovery) {
						cantry = true;
						mainFrame.getLogFrame().publish(new LogRecord(Level.INFO, getName() + " is retriable! Executing again! \n"));
						continue;
					} else if(false) {
						//find substitute
					} else {
						if(!mainFrame.getExecutionProperties().isForwardRecoveryEnabled()) {
							
							if(mainFrame.getExecutionProperties().isCheckpointingEnabled()) {
								changeState(EXECUTION_STATE.ABANDONED);
								outputs.clear();
								
								outputs.put(new Data("executer_message"),ExecuterConstants.Messages.CHECKPOINT_TOKEN.toString());
								System.out.println(getName() + " sends " + ExecuterConstants.Messages.CHECKPOINT_TOKEN );
								sendOutputs();
								checkpointed = true;
								cantry = false;
							}
								
							} else {
								changeState(EXECUTION_STATE.ABANDONED);
				
								//compensate
								
								outputs.clear();
								
								outputs.put(new Data("executer_message"),ExecuterConstants.Messages.COMPENSATE.toString());
								System.out.println(getName() + " sends " + ExecuterConstants.Messages.COMPENSATE + " to " + Constants.FINAL_NODE + " for compensation");
								manager.getEngineThreads().get(Constants.FINAL_NODE).addInputs(outputs);
								System.out.println(getName() + " finished sending " + ExecuterConstants.Messages.COMPENSATE + " to " + Constants.FINAL_NODE);
								
								compensate();
								cantry = false;
								
							}
					}
				}
	        } while(!success && cantry);
	        
	        
	        System.out.println(getName() + " finished its execution");
	        
        } 

        if(service.isControl()) {
        	System.out.println("finished " + getName());
        	showOutputs();
        	if(isCheckpointed())
        		checkpoint();
        }
        

    	mainFrame.getCheckpointData().setInputs(service, inputs);
    	mainFrame.getCheckpointData().setOutputs(service, outputs);
    	if(service.isControl())
    		System.out.println(mainFrame.getCheckpointData());

    	
        	
    }

	private void checkpoint() {
		JOptionPane.showMessageDialog(null, " The CWS failed and it has been checkpointed!");	
	}


	public boolean isCheckpointed() {
		return checkpointed;
	}

	private void doSimulation() throws SimulationException {
		Service serviceSim = mainFrame.getSimulationProperties().getService(service.getName());
		if(serviceSim != null) {
			if(mainFrame.getSimulationProperties().isSimulateAvailability()) {
				if(Math.random() < ((serviceSim.getFailureProbability())))
					   throw new SimulationException(serviceSim.getName() + " has failed");
				
			}
		}
		
	}


	private void compensate() {
		System.out.println("Compensating " + getName() + ", state: " + service.getExecutionState());
		setCompensationOutputs();
		
		if(service.isAbandoned()) {
			//it is the WS that failed and started the compensation. Do nothing.
			inputs.clear();
			waitForInputs();
			
		} else if(service.isExecuted()) {
			//this WS was successfully executed. It must be compensated
			inputs.clear();
			waitForInputs();
			try {
				changeState(EXECUTION_STATE.RUNNING_COMPENSATION);
				Thread.sleep((long) 5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mainFrame.getLogFrame().publish(new LogRecord(Level.INFO, getName() + " was compensated!\n"));
			//inputs.clear();
			
		} else if(service.isInitialState()) {
			//this WS was never executed. Do nothing.
			inputs.clear();
			if(service.isFinal()) {
				outputs.clear();
				outputs.put(new Data("executer_message"),ExecuterConstants.Messages.COMPENSATE_TOKEN.toString());
				for(Service succ:mainFrame.getGraph().getPredecessors(service)) {
					System.out.println(getName() + " sends " + outputs + " to " + succ.getName());
					manager.getEngineThreads().get(succ.getName()).addInputs(this.outputs);
		    	}
				
			}
			waitForInputs();
		}
		
		if(!service.isControl()) {
			outputs.clear();
			outputs.put(new Data("executer_message"),ExecuterConstants.Messages.COMPENSATE_TOKEN.toString());
			for(Service succ:mainFrame.getGraph().getPredecessors(service)) {
				System.out.println(getName() + " sends " + outputs + " to " + succ.getName());
				if(succ.getName().equals(Constants.INITIAL_NODE))
					manager.getEngineThreads().get(Constants.FINAL_NODE).addInputs(this.outputs);
				else
					manager.getEngineThreads().get(succ.getName()).addInputs(this.outputs);
	    	}
			
			System.out.println(getName() + " finished compensation");
		} 
			
		
		
	}

	private void sendOutputs() {
		
		for(Service succ:mainFrame.getGraph().getSuccessors(service)) {
			
			manager.getEngineThreads().get(succ.getName()).addInputs(this.outputs);
    	}
		
	}
	
	private boolean isFireable() {
		try {
			return inputs.size() == service.getInputs().size() || stopWaiting;
		} finally {
			stopWaiting = false;
		}
	}
	
	private boolean producedAllOutputs() {
		return outputs.size() == service.getOutputs().size();
	}

	private void waitForInputs() {
		lock.lock();
		try {
			System.out.println(getName() + " is gonna wait for inputs: " + service.getInputs());
			while(!isFireable()) {
				System.out.println(getName() + " is waiting for inputs: " + service.getInputs());
				notFull.await();
			}
				
			System.out.println(getName() + " received all inputs: " + inputs);
		} catch (InterruptedException e) {
			System.out.println("waitForInputs: InterruptedException");
		} finally {
			lock.unlock();
		}
		
	}
	
	public void addInputs(Map<Data, String> message) {
		lock.lock();
		try {
			System.out.println(getName() + " received: " + message);
			//verify input
			
			if(message.containsValue(ExecuterConstants.Messages.COMPENSATE.toString())) {

				
				if(service.isFinal()) {
					
					for(Service s:mainFrame.getGraph().getVertices()) {
						System.out.println(mainFrame.getGraph().getVertexCount());
						if(!s.isControl() && !s.getName().equals("MSTranslate")) {
							outputs.clear();
							outputs.put(new Data("executer_message"),ExecuterConstants.Messages.COMPENSATE.toString());
							System.out.println(getName() + " sends " + ExecuterConstants.Messages.COMPENSATE + " to " + s.getName());
							manager.getEngineThreads().get(s.getName()).addInputs(outputs);
							System.out.println(getName() + " sent " + ExecuterConstants.Messages.COMPENSATE + " to " + s.getName());
						}
					}
					System.out.println("wscf finished sending compensated to all");
				}
				
				inputs.clear();
				toCompensate = true;
			} else {

				this.inputs.putAll(message);
			}
		} finally {
			
			notFull.signal();
			lock.unlock();
			stopWaiting = true;
		}
	}
	
	
	private void setCompensationOutputs() {
		service.setInputs(new ArrayList<Data>());
		service.setOutputs(new ArrayList<Data>());
		
		if(!service.isControl()) {
			for(int i=0; i<mainFrame.getGraph().getSuccessorCount(service); i++)
				service.addInput(new Data("executer_message"));
		} else {
			for(int i=0; i<mainFrame.getGraph().getPredecessorCount(service); i++)
				service.addInput(new Data("executer_message"));
		}
		
	}


	private void showOutputs() {
		 showOutputsDialog = new ShowOutputsDialog(mainFrame, this.inputs);
		 showOutputsDialog.pack();
		 showOutputsDialog.setLocationRelativeTo(this.mainFrame);
		 showOutputsDialog.setVisible(true);
	}
	
	private void changeState(EXECUTION_STATE state) {
		service.setExecutionState(state);
		mainFrame.getCwsFrame().repaint();
	}
	
	public Map<Data, String> getInputs() {
		return inputs;
	}
	
	public Map<Data, String> getOutputs() {
		return outputs;
	}


}