package fr.dauphine.enginethread;

public class ExecuterConstants {
	
	public enum Messages {
		COMPENSATE, //initiate the compensation
		COMPENSATE_TOKEN, //token to unroll the compensation
		CHECKPOINT,
		CHECKPOINT_TOKEN
	}
	
	public static boolean isMessage(String string) {
		boolean isMessage = false;
		
		isMessage = string.equals(Messages.COMPENSATE) ||
				 string.equals(Messages.COMPENSATE_TOKEN) ||
				 string.equals(Messages.CHECKPOINT) ||
				 string.equals(Messages.CHECKPOINT_TOKEN);
		
		return isMessage;
	}

}
