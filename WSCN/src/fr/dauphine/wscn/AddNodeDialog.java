package fr.dauphine.wscn;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import edu.uci.ics.jung.algorithms.layout.util.Relaxer;
import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Service;
import fr.dauphine.ws.Registry;

import java.beans.*; //property change stuff
import java.util.List;
import java.awt.*;
import java.awt.event.*;

public class AddNodeDialog extends JDialog
implements ActionListener, ListSelectionListener,
PropertyChangeListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String typedText = null;
	private JTextField textField;
	//private DialogDemo dd;
	private JList list, list2;
    private DefaultListModel listModel;
    JScrollPane listScrollPane, listScrollPane2;

	private JOptionPane optionPane;
	private JComboBox<String> comboBox;

	private String btnString1 = "Enter";
	private String btnString2 = "Cancel";
	private Frame frame;
	private MainFrame wscn;

	/**
	 * Returns null if the typed string was invalid;
	 * otherwise, returns the string as the user entered it.
	 */
	public String getValidatedText() {
		return typedText.toString();
	}

	/** Creates the reusable dialog. 
	 * @param g 
	 * @param g */
	public AddNodeDialog(MainFrame mainFrame) {
		super(mainFrame, true);
		this.wscn = mainFrame;
		//dd = parent;
		listModel = new DefaultListModel();
		//Create the list and put it in a scroll pane.
		list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		list.setSelectedIndex(0);
		list.addListSelectionListener(this);
		list.setVisibleRowCount(5);
		listScrollPane = new JScrollPane(list);
		list2 = new JList(listModel);
		list2.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		list2.setSelectedIndex(0);
		list2.addListSelectionListener(this);
		list2.setVisibleRowCount(5);
		listScrollPane2 = new JScrollPane(list2);
		
		
		setTitle("New Service");

		
		// Options in the combobox
		String[] optionsCombobox = Registry.serviceClasses.keySet().toArray(new String[Registry.serviceClasses.keySet().size()]);
        comboBox = new JComboBox<String>(optionsCombobox);
        comboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // Do something when you select a value

            }
        });

		//Create an array of the text and components to be displayed.
		Object[] array = {new JLabel("Service:"), comboBox, 
				new JLabel("Predecessors:"), listScrollPane, 
				new JLabel("Successors:"), listScrollPane2};

		//Create an array specifying the number of dialog buttons
		//and their text.
		Object[] options = {btnString1, btnString2};

		//Create the JOptionPane.
		optionPane = new JOptionPane(array,
				JOptionPane.QUESTION_MESSAGE,
				JOptionPane.YES_NO_OPTION,
				null,
				options,
				options[0]);

		//Make this dialog display it.
		//optionPane = new JOptionPane();
		setContentPane(optionPane);
		//add(listScrollPane, BorderLayout.CENTER);
		//add(listScrollPane2, BorderLayout.CENTER);
		

		//Handle window closing correctly.
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				/*
				 * Instead of directly closing the window,
				 * we're going to change the JOptionPane's
				 * value property.
				 */
				optionPane.setValue(new Integer(
						JOptionPane.CLOSED_OPTION));
			}
			public void windowActivated(WindowEvent e) {
                listModel.clear();
                if(wscn.getGraph() != null) {
                	for(Service s:wscn.getGraph().getVertices())
                		listModel.addElement(s);
                }
            }
		});

		//Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter() {
			public void componentShown(ComponentEvent ce) {
				comboBox.requestFocusInWindow();
			}
		});

		//Register an event handler that puts the text into the option pane.
		//textField.addActionListener(this);

		//Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
	}

	/** This method handles events for the text field. */
	public void actionPerformed(ActionEvent e) {
		optionPane.setValue(btnString1);
		
	}

	/** This method reacts to state changes in the option pane. */
	public void propertyChange(PropertyChangeEvent e) {

		String prop = e.getPropertyName();
		
		if (isVisible()
				&& (e.getSource() == optionPane)
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) ||
						JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
			Object value = optionPane.getValue();
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				//ignore reset
				return;
			}

			//Reset the JOptionPane's value.
			//If you don't do this, then if the user
			//presses the same button next time, no
			//property change event will be fired.
			optionPane.setValue(
					JOptionPane.UNINITIALIZED_VALUE);

			if (btnString1.equals(value)) {
				typedText = (String) comboBox.getSelectedItem();
				String ucText = typedText.toString();
				addNode(Registry.getService(ucText)
				, list.getSelectedValuesList()
				, list2.getSelectedValuesList());
				
				if (true) {
					wscn.getInformationFrame().updateInformation();
					System.out.println("We are done");
					//we're done; clear and dismiss the dialog
					clearAndHide();
				} else {
					//the graph contains cycles
					textField.selectAll();
					JOptionPane.showMessageDialog(
							AddNodeDialog.this,
							"The CWS must not contain cycles"
									+ "\n"
									+ "Please try again ",
									"Please Try again",
									JOptionPane.ERROR_MESSAGE);
					typedText = null;
					textField.requestFocusInWindow();
				}
			} else { //user closed dialog or clicked cancel
				/*dd.setLabel("It's OK.  "
						+ "We won't force you to type "
						+ magicWord + ".");
				typedText = null;*/
				clearAndHide();
			}
		}
	}
	
	protected void addNode(Service service, List pred, List succ) {

        try {
        	
        	wscn.getCwsFrame().layout.lock(true);
            //add a vertex
            Integer i1 = new Integer(wscn.getGraph().getVertexCount());

            Relaxer relaxer = wscn.getCwsFrame().getVv().getModel().getRelaxer();
            relaxer.pause();
            wscn.getGraph().addVertex(service);
            System.err.println("added node " + service);
            
            //adding edges from predecessors
            for(Object p:pred)
            	wscn.getGraph().addEdge(wscn.getGraph().getEdgeCount(), (Service)p, service);
    
            //adding edges to successors
            for(Object p:succ)
            	wscn.getGraph().addEdge(wscn.getGraph().getEdgeCount(), service, (Service)p);

            // wire it to some edges
        	boolean hasCycles = false;
        	//g.addEdge(g.getEdgeCount(), v_prev, v1);
            // let's connect to a random vertex, too!
            //int rand = (int) (Math.random() * g.getVertexCount());
            //g.addEdge(g.getEdgeCount(), v1, new Service(new Integer(rand).toString()));
            hasCycles = GraphUtils.hasCycles(wscn.getGraph());
            System.out.println("hasCycles " + hasCycles);
            if(hasCycles)
            	wscn.getGraph().removeEdge(wscn.getGraph().getEdgeCount()-1);

            wscn.getCwsFrame().layout.initialize();
            relaxer.resume();
            wscn.getCwsFrame().layout.lock(false);

        } catch (Exception e) {
            System.out.println(e);

        }
    }
	
	//This method is required by ListSelectionListener.
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {
 
            if (list.getSelectedIndex() == -1) {
            //No selection, disable fire button.
                //fireButton.setEnabled(false);
 
            } else {
            //Selection, enable the fire button.
                //fireButton.setEnabled(true);
            }
        }
    }

	/** This method clears the dialog and hides it. */
	public void clearAndHide() {
		//textField.setText(null);
		setVisible(false);
	}
}