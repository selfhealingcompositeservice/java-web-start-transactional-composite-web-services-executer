package fr.dauphine.wscn;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.*;
import java.awt.*;

/* Used by InternalFrameDemo.java. */
public class RequiredQoSFrame extends JInternalFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;

    MainFrame mainFrame;
    
    private JLabel priceLabel;
    
    JTextField priceValue;
    JTextField executionTimeValue;
    JTextField availabilityValue;
    JTextField reputationValue;
    
    static final String NOT_SET = "Not set";
    
    public RequiredQoSFrame(MainFrame mainFrame) {
        super("Required QoS", 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable
        this.mainFrame = mainFrame;
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(200,300);
        setLocation(new Double(mainFrame.getCwsFrame().getLocation().getX()).intValue() + mainFrame.getCwsFrame().getSize().width, 
        		InformationFrame.getHeightValue());
        
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        getContentPane().add(new JLabel("Price: "));
        //getContentPane().add(Box.createHorizontalGlue());
        priceValue = new JTextField("0");
        priceValue.setEnabled(true);
        getContentPane().add(priceValue);
        
        //reputation
        getContentPane().add(new JLabel("Reputation: "));
        //getContentPane().add(Box.createHorizontalGlue());
        reputationValue = new JTextField("0");
        reputationValue.setEnabled(true);
        getContentPane().add(reputationValue);
        
        //availability
        getContentPane().add(new JLabel("Availability: "));
        //getContentPane().add(Box.createHorizontalGlue());
        availabilityValue = new JTextField("0");
        availabilityValue.setEnabled(true);
        getContentPane().add(availabilityValue);
        
        //execution time
        getContentPane().add(new JLabel("Execution Time: "));
        //getContentPane().add(Box.createHorizontalGlue());
        executionTimeValue = new JTextField("0");
        executionTimeValue.setEnabled(true);
        getContentPane().add(executionTimeValue);
        
        //transactional property
        //getContentPane().add(new JLabel("Transactional Property: "));
        //getContentPane().add(Box.createHorizontalGlue());
        /*JTextField transactionalProperty = new JTextField("0");
        transactionalProperty.setEnabled(false);
        getContentPane().add(transactionalProperty);*/

        //Set the window's location.
        //setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
           /*priceLabel = new JLabel("Price: ");
        getContentPane().add(priceLabel);
        priceValue = new JLabel("0");
        getContentPane().add(priceValue);*/
        init();
    }
    
    public void init() {
    	priceValue.setText("NOT_SET");
        reputationValue.setText("NOT_SET");
        availabilityValue.setText("NOT_SET");
        executionTimeValue.setText("NOT_SET");
    }
}
