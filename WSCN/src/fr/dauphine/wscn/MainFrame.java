package fr.dauphine.wscn;



import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.ObservableGraph;
import fr.dauphine.checkpointing.CheckpointData;
import fr.dauphine.executer.ExecutionManager;
import fr.dauphine.file.FileGraph;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.properties.ExecutionProperties;
import fr.dauphine.properties.SimulationProperties;
import fr.dauphine.service.Service;
import fr.dauphine.ws.Registry;

/*
 * 
 */
public class MainFrame  extends JFrame
implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JDesktopPane desktop;
	
	//Frames
	private CWSFrame cwsFrame;
	private RequiredQoSFrame requiredQoSFrame;
	private OptionsFrame optionsframe;
	private InformationFrame informationFrame;
	private LogFrame logFrame;
	
	
	private Graph<Service,Number> g = null;
	
	SimulationDialog simulationDialog;
	private SimulationProperties simulationProperties;
	
	ExecutionPropertiesDialog executionPropertiesDialog;
	private ExecutionProperties executionProperties;
	private CheckpointData checkpointData;
	
	JFileChooser fc;
	
	public Graph<Service,Number> getGraph() {
		return g;
	}
	
	

	public MainFrame() {
		super("Composite Web Service Execution");
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			JFrame.setDefaultLookAndFeelDecorated(true);
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Registry initialization
		Registry.init();

		manager = new ExecutionManager(this);
		
		//Make the big window be indented 50 pixels from each edge
		//of the screen.
		int inset = 50;
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(inset, inset,
				screenSize.width  - inset*2,
				screenSize.height - inset*2);
		
		setExtendedState(getExtendedState()|JFrame.MAXIMIZED_BOTH );

		//Set up the GUI.
		desktop = new JDesktopPane(); //a specialized layered pane
		createFrame(); //create first "window"
		createOptionsFrame();
		createInformationFrame();
		createRequiredQoSFrame();
		createLogFrame();
		setContentPane(desktop);
		setJMenuBar(createMenuBar());

		//Make dragging a little faster but perhaps uglier.
		desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
	}

	protected JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();

		//Set up the lone menu.
		JMenu menu = new JMenu("File");
		menu.setMnemonic(KeyEvent.VK_F);
		menuBar.add(menu);

		//Set up the first menu item.
		JMenuItem openFile = new JMenuItem("Open File...");
		openFile.setActionCommand("openFile");
		openFile.addActionListener(this);
		menu.add(openFile);
		
		JMenuItem saveAs = new JMenuItem("Save As...");
		saveAs.setActionCommand("saveAs");
		saveAs.addActionListener(this);
		menu.add(saveAs);

		//Set up the second menu item.
		JMenuItem menuItem = new JMenuItem("Quit");
		menuItem.setMnemonic(KeyEvent.VK_Q);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_Q, ActionEvent.ALT_MASK));
		menuItem.setActionCommand("quit");
		menuItem.addActionListener(this);
		menu.add(menuItem);
		
		JMenu tools = new JMenu("Tools");
		tools.setMnemonic(KeyEvent.VK_T);
		menuBar.add(tools);
		
		JMenuItem simulation = new JMenuItem("Simulation");
		simulation.setMnemonic(KeyEvent.VK_S);
		simulation.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_S, ActionEvent.ALT_MASK));
		simulation.setActionCommand("simulation");
		simulation.addActionListener(this);
		tools.add(simulation);
		
		JMenuItem properties = new JMenuItem("Execution Properties");
		properties.setMnemonic(KeyEvent.VK_P);
		properties.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_P, ActionEvent.ALT_MASK));
		properties.setActionCommand("properties");
		properties.addActionListener(this);
		tools.add(properties);
		
		JMenu help = new JMenu("Help");
		help.setMnemonic(KeyEvent.VK_H);
		menuBar.add(help);
		
		JMenuItem about = new JMenuItem("About");
		about.setMnemonic(KeyEvent.VK_A);
		about.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_A, ActionEvent.ALT_MASK));
		about.setActionCommand("about");
		about.addActionListener(this);
		help.add(about);
		
		JMenuItem system = new JMenuItem("System");
		about.setMnemonic(KeyEvent.VK_S);
		system.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_S, ActionEvent.ALT_MASK));
		system.setActionCommand("system");
		system.addActionListener(this);
		help.add(system);

		return menuBar;
	}

	//React to menu selections.
	public void actionPerformed(ActionEvent e) {
		if ("new".equals(e.getActionCommand())) { //new
			createFrame();
		} else if ("about".equals(e.getActionCommand())) {
			createAboutFrame();
		} else if ("simulation".equals(e.getActionCommand())) {
			createSimulationDialog();
		} else if ("system".equals(e.getActionCommand())) {
			createSystemDialog();
		} else if ("properties".equals(e.getActionCommand())) {
			createExecutioPropertiesDialog();
		} else if ("openFile".equals(e.getActionCommand())) {
			openFile();
		} else if ("saveAs".equals(e.getActionCommand())) {
			saveAs();
		} else //quit
			quit();
	}

	
	public void saveAs() {
		if(fc == null)
			fc = new JFileChooser();
		
		 int returnVal = fc.showSaveDialog(this);
         if (returnVal == JFileChooser.APPROVE_OPTION) {
             File file = fc.getSelectedFile();
             System.out.println("Saving graph :" + file.getName());
             FileGraph.saveGraph(getGraph(), file.getAbsolutePath(), this);
         }
		
	}



	private void openFile() {
		if(fc == null)
			fc = new JFileChooser();
		
		int returnVal = fc.showOpenDialog(this);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			//reset graph
			cwsFrame.clearGraph();
            
            File file = fc.getSelectedFile();
            FileGraph.loadGraph(file.getAbsolutePath(), getGraph());
            System.out.println(getGraph());
            informationFrame.updateInformation();
        } else {
            //log.append("Open command cancelled by user." + newline);
        }
		
	}



	private void createSimulationDialog() {
		if(simulationDialog == null) {
			simulationDialog = new SimulationDialog(this);
			simulationProperties = new SimulationProperties();
		}
		simulationDialog.setLocationRelativeTo(this);
		simulationDialog.setVisible(true);
	}

	private void createExecutioPropertiesDialog() {
		if(executionPropertiesDialog == null) {
			executionPropertiesDialog = new ExecutionPropertiesDialog(this);
			//executionProperties = new ExecutionProperties();
		}
		executionPropertiesDialog.setLocationRelativeTo(this);
		executionPropertiesDialog.setVisible(true);
	}


	
	protected void createSystemDialog() {
		SystemDialog systemFrame = new SystemDialog(this);
		systemFrame.setLocationRelativeTo(this);
		systemFrame.setVisible(true); //necessary as of 1.3
	}
	
	protected void createAboutFrame() {
		AboutDialog aboutFrame = new AboutDialog(this);
		aboutFrame.setLocationRelativeTo(this);
		aboutFrame.setVisible(true); //necessary as of 1.3
	}
	
	//Create a new internal frame.
	protected void createFrame() {
		this.cwsFrame = new CWSFrame(this);
		getCwsFrame().setVisible(true); //necessary as of 1.3
		desktop.add(getCwsFrame());
		try {
			getCwsFrame().setSelected(true);
		} catch (java.beans.PropertyVetoException e) {}
	}
	
	
	
	protected void createOptionsFrame() {
		this.optionsframe = new OptionsFrame(this);
		getOptionsframe().setVisible(true); //necessary as of 1.3
		desktop.add(getOptionsframe()); 
		try {
			getOptionsframe().setSelected(true);
		} catch (java.beans.PropertyVetoException e) {}
	}
	
	
	protected void createInformationFrame() {
		informationFrame = new InformationFrame(this);
		informationFrame.setVisible(true); //necessary as of 1.3
		desktop.add(informationFrame);
		try {
			informationFrame.setSelected(true);
		} catch (java.beans.PropertyVetoException e) {}
	}
	
	protected void createRequiredQoSFrame() {
		requiredQoSFrame = new RequiredQoSFrame(this);
		requiredQoSFrame.setVisible(true); //necessary as of 1.3
		desktop.add(requiredQoSFrame);
		try {
			requiredQoSFrame.setSelected(true);
		} catch (java.beans.PropertyVetoException e) {}
	}
	
	protected void createLogFrame() {
		logFrame = new LogFrame();
		//logFrame.setVisible(true); //necessary as of 1.3
		desktop.add(logFrame.getFrame());
	}
	
	public InformationFrame getInformationFrame() {
		return informationFrame;
	}

	//Quit the application.
	protected void quit() {
		System.exit(0);
	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	private static void createAndShowGUI() {
		//Make sure we have nice window decorations.
		JFrame.setDefaultLookAndFeelDecorated(true);

		//Create and set up the window.
		MainFrame frame = new MainFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Display the window.
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		System.out.println("starting");
		//Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public void setGraph(ObservableGraph<Service, Number> og) {
		this.g = og;
		
	}

	public CWSFrame getCwsFrame() {
		return cwsFrame;
	}

	public OptionsFrame getOptionsframe() {
		return optionsframe;
	}
	
	public RequiredQoSFrame getRequiredQoSFrame() {
		return requiredQoSFrame;
	}

	public LogFrame getLogFrame() {
		return logFrame;
	}

	public void setLogFrame(LogFrame logFrame) {
		this.logFrame = logFrame;
	}
	
	public Service getVertexByName(String name) {
		return GraphUtils.getVertexByName(getGraph(), name);
	}



	public SimulationProperties getSimulationProperties() {
		if(simulationProperties == null)
			simulationProperties = new SimulationProperties();
		return simulationProperties;
	}
	
	public ExecutionProperties getExecutionProperties() {
		if(executionProperties == null)
			executionProperties = new ExecutionProperties();
		return executionProperties;
	}
	
	public CheckpointData getCheckpointData() {
		if(checkpointData == null)
			checkpointData = new CheckpointData();
		return checkpointData;
	}


	ExecutionManager manager;
	
	public ExecutionManager getExecutionManager() {
		
		return manager;
	}



	public void execute() {
		manager.execute();
		
	}
	
}