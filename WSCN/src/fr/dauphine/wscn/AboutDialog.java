package fr.dauphine.wscn;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.beans.*; //property change stuff
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.TextArea;
import java.awt.event.*;

public class AboutDialog extends JDialog
implements ActionListener, ListSelectionListener,
PropertyChangeListener, HyperlinkListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JEditorPane epBrowser;

	/** Creates the reusable dialog. 
	 * @param g 
	 * @param g */
	public AboutDialog(MainFrame mainFrame) {
		super(mainFrame, true);
		setTitle("About");
		setSize(500, 200);
        setMinimumSize(new Dimension(500, 200));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		
		JPanel pan=new JPanel();
		pan.setLayout(new FlowLayout());

        epBrowser = new JEditorPane();
        epBrowser.setSize(500, 200);
        epBrowser.setMinimumSize(new Dimension(500, 200));
        
        // this is the trick!
        epBrowser.setEditable(false);
        epBrowser.addHyperlinkListener(this);    


        epBrowser.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        
        epBrowser.setContentType("text/html");
        try {
        	File file = new File("about.html");
			epBrowser.setPage(file.toURI().toURL());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        pan.add(epBrowser);
        add(pan);
        pack();
    }// </editor-fold>


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
	
		
	}


	@Override
	public void hyperlinkUpdate(HyperlinkEvent event) {
		try
        {
            
            Desktop.getDesktop().browse(new URI("http://www.lamsade.dauphine.fr/~angarita/dynamicse.html"));

        }
        catch (Throwable e)
        {
            JOptionPane.showMessageDialog(null, "Sorry, can't launch a browser.");
        }
		
	}
}