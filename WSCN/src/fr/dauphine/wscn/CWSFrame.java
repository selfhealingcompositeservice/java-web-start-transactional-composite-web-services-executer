package fr.dauphine.wscn;

import edu.uci.ics.jung.algorithms.layout.AbstractLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout2;
import edu.uci.ics.jung.algorithms.layout.SpringLayout;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.ObservableGraph;
import edu.uci.ics.jung.graph.event.GraphEvent;
import edu.uci.ics.jung.graph.event.GraphEventListener;
import edu.uci.ics.jung.graph.util.Graphs;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.annotations.AnnotatingGraphMousePlugin;
import edu.uci.ics.jung.visualization.annotations.AnnotatingModalGraphMouse;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.GraphMouseListener;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.renderers.ReshapingEdgeRenderer;
import fr.dauphine.enginethread.EngineThread;
import fr.dauphine.enginethread.ExecuterConstants;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;
import fr.dauphine.service.Constants.EXECUTION_STATE;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ConstantTransformer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;

/**
 * Composite Web Service Execution System Demo.
 *
 * @author Rafael Angarita
 */
public class CWSFrame extends JInternalFrame {

    /**
	 *
	 */
	static JFrame frame;
	
	private static final long serialVersionUID = -5345319851341875800L;

	//private Graph<Service,Number> g = null;

    private VisualizationViewer<Service,Number> vv = null;

    protected AbstractLayout<Service,Number> layout = null;

    Timer timer;

    boolean done;

    protected JButton switchLayout;
    

    public static final int EDGE_LENGTH = 100;
    
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    MainFrame mainFrame;

    public CWSFrame(MainFrame mainFrame) {
        super("Composite Web Service", 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable
        this.mainFrame = mainFrame;
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(1000,500);
        
        //Set the window's location.
        setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
        init();
        start();
    }


    public void init() {
    	
    	

        //create a graph
    	Graph<Service,Number> ig = Graphs.<Service,Number>synchronizedDirectedGraph(new DirectedSparseMultigraph<Service,Number>());

        ObservableGraph<Service,Number> og = new ObservableGraph<Service,Number>(ig);
        og.addGraphEventListener(new GraphEventListener<Service,Number>() {

			public void handleGraphEvent(GraphEvent<Service, Number> evt) {
				System.err.println("got "+evt);
				//if(evt.getType().equals())
				repaint();
			}});
        mainFrame.setGraph(og);
        
        
        
        //create a graphdraw
        layout = new FRLayout2<Service,Number>(mainFrame.getGraph());
//        ((FRLayout)layout).setMaxIterations(200);

        setVv(new VisualizationViewer<Service,Number>(layout, new Dimension(400,400)));
        
     // Transformer maps the vertex number to a vertex property
        Transformer<Service,Paint> vertexColor = new Transformer<Service,Paint>() {
            public Paint transform(Service i) {
            	System.out.println("Change color: " + i.getName() + " " + i.isAbandoned());
                if(i.getExecutionState().equals(EXECUTION_STATE.RUNNING)) {
                	
                	return new Color(97, 255, 91);
                } else if(i.getExecutionState().equals(EXECUTION_STATE.RUNNING_COMPENSATION)) {
                	return new Color(250, 164, 245);
                } else if(i.isControl()) {
                	return new Color(247, 197, 197);
                } else if(i.isExecuted()) {
                	return new Color(213, 199, 199);
                } else if(i.isAbandoned()) {
                	return new Color(255,165,0);
                }

                return new Color(252, 101, 101);
            }
        };
        Transformer<Service,Shape> vertexSize = new Transformer<Service,Shape>(){
            public Shape transform(Service i){
            	if(!i.isControl())
            		return new Ellipse2D.Double(-15, -15, 100, 100);
            	else
            		return new Ellipse2D.Double(-15, -15, 100, 100);
                
            }
        };
        getVv().getRenderContext().setVertexFillPaintTransformer(vertexColor);
        getVv().getRenderContext().setVertexShapeTransformer(vertexSize);
        
        
        JRootPane rp = this.getRootPane();
        rp.putClientProperty("defeatSystemEventQueueCheck", Boolean.TRUE);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().setBackground(java.awt.Color.lightGray);
        getContentPane().setFont(new Font("Serif", Font.BOLD, 12));

        getVv().getModel().getRelaxer().setSleepTime(500);
        getVv().setGraphMouse(new DefaultModalGraphMouse<Service,Number>());

  
        getVv().getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR);
        getVv().getRenderContext().setVertexLabelTransformer(new ToStringLabeller<Service>());
        getVv().setForeground(Color.white);
        getContentPane().add(getVv());
        switchLayout = new JButton("Switch to SpringLayout");
        switchLayout.addActionListener(new ActionListener() {

            @SuppressWarnings("unchecked")
            public void actionPerformed(ActionEvent ae) {
            	Dimension d = new Dimension(600,600);
                if (switchLayout.getText().indexOf("Spring") > 0) {
                    switchLayout.setText("Switch to FRLayout");
                    layout = new SpringLayout<Service,Number>(mainFrame.getGraph(),
                        new ConstantTransformer(EDGE_LENGTH));
                    layout.setSize(d);
                    getVv().getModel().setGraphLayout(layout, d);
                } else {
                    switchLayout.setText("Switch to SpringLayout");
                    layout = new FRLayout<Service,Number>(mainFrame.getGraph(), d);
                    getVv().getModel().setGraphLayout(layout, d);
                }
            }
        });
        
        RenderContext<Service,Number> rc = vv.getRenderContext();
        AnnotatingGraphMousePlugin<Service,Number> annotatingPlugin =
        new AnnotatingGraphMousePlugin<Service,Number>(rc);

        //getContentPane().add(switchLayout, BorderLayout.SOUTH);
     // create a GraphMouse for the main view
        // 
        final AnnotatingModalGraphMouse<Service,Number> graphMouse = 
        new AnnotatingModalGraphMouse<Service,Number>(rc, annotatingPlugin);
        vv.setGraphMouse(graphMouse);
        vv.addKeyListener(graphMouse.getModeKeyListener());
        graphMouse.setMode(edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode.PICKING);

        timer = new Timer();
        
        vv.addGraphMouseListener(new GraphMouseListener<Service>() {

        	@Override
        	public void graphClicked(Service v, MouseEvent me) {
        		if (me.getButton() == MouseEvent.BUTTON1 && me.getClickCount() == 2) {
        			showWSProperties(v);
        		}
        		me.consume();
        	}


			@Override
        	public void graphPressed(Service v, MouseEvent me) {
        	}

        	@Override
        	public void graphReleased(Service v, MouseEvent me) {
        	}
        });
    }
    public void start() {
        validate();
        //set timer so applet will change
        timer.schedule(new RemindTask(), 1000, 1000); //subsequent rate
        getVv().repaint();
    }

    Service v_prev = null;

    private void showWSProperties(Service s) {
    	if(s.isControl())
    		 JOptionPane.showMessageDialog(null, s.getName() + " is a control node! \n It has no properties.");
    	else {
			WSPropertiesDialog wsProperties = new WSPropertiesDialog(mainFrame, s);
			wsProperties.setLocationRelativeTo(this);
			wsProperties.setVisible(true);
    	}
		
	}

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            //process();
            //if(done) cancel();

        }
    }
    
    /*private JPanel createPane(String description,
            JRadioButton[] radioButtons,
            JButton showButton) {

		int numChoices = radioButtons.length;
		JPanel box = new JPanel();
		JLabel label = new JLabel(description);
		
		box.setLayout(new BoxLayout(box, BoxLayout.PAGE_AXIS));
		box.add(label);
		
		for (int i = 0; i < numChoices; i++) {
		box.add(radioButtons[i]);
		}
		
		JPanel pane = new JPanel(new BorderLayout());
		pane.add(box, BorderLayout.PAGE_START);
		pane.add(showButton, BorderLayout.PAGE_END);
		return pane;
	}*/


	public VisualizationViewer<Service,Number> getVv() {
		return vv;
	}


	public void setVv(VisualizationViewer<Service,Number> vv) {
		this.vv = vv;
	}
	
	public void clearGraph() {
		List<Service> toRemove = new ArrayList<>();
        for(Service s:mainFrame.getGraph().getVertices())
        	toRemove.add(s);
        for(Service s:toRemove)
        	mainFrame.getGraph().removeVertex(s);
	}
    
	
   /* protected AbstractLayout<Service,Number> getLayout() {
		return layout;
	}*/
    
    
    /*public int compare(Number a, Number b){
        return new BigDecimal(a.toString()).compareTo(new BigDecimal(b.toString()));
    }*/

//    public static void main(String[] args) {
//    	CWSFrame and = new CWSFrame();	
//        
//    	frame = new JFrame();
//        
//        
//    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//    	frame.getContentPane().add(and);
//
//    	and.init();
//    	and.start();
//    	frame.pack();
//    	frame.setVisible(true);
//    }
}