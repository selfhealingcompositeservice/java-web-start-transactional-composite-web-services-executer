package fr.dauphine.wscn;

public final class Constants {
	public static final int EXECUTION_ENGINE = 0;
	public enum Messages {
	    INITIALIZE, FINALIZE, DATA;
	    
	    public static boolean equals(Constants.Messages m, Object o) {
	    	if(o instanceof Constants.Messages)
				return m.equals((Constants.Messages)o);
			return false;
	    }
	}
}