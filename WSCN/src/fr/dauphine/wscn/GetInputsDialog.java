package fr.dauphine.wscn;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.dauphine.message.DataMessage;
import fr.dauphine.service.Data;

public class GetInputsDialog extends JDialog
implements ActionListener, ListSelectionListener,
PropertyChangeListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String typedText = null;

	private JOptionPane optionPane;

	private String btnString1 = "Enter";
	private String btnString2 = "Cancel";

	private Object[] array;
	
	List<DataMessage> list;
	
	/**
	 * Returns null if the typed string was invalid;
	 * otherwise, returns the string as the user entered it.
	 */
	public String getValidatedText() {
		return typedText.toString();
	}

	/** Creates the reusable dialog. 
	 * @param list 
	 * @param g 
	 * @param g */
	public GetInputsDialog(MainFrame mainFrame, List<DataMessage> list) {
		super(mainFrame, true);
		this.list = list;
		setSize(200,300);
		setTitle("CWS Inputs");
		
		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		array = new Object[list.size()*2];
		
		int i=0;
		for(DataMessage dm:list) {
	        JTextField text = new JTextField("");
	        text.setName(dm.getData().getName());
	        text.setEnabled(true);
	        array[i++] = new JLabel(dm.getData().getName() + "(" + dm.getReceiverService()  + ")");
	        array[i++] = text;
		}
        
		//Create an array specifying the number of dialog buttons
		//and their text.
		Object[] options = {btnString1, btnString2};

		//Create the JOptionPane.
		optionPane = new JOptionPane(array,
				JOptionPane.QUESTION_MESSAGE,
				JOptionPane.YES_NO_OPTION,
				null,
				options,
				options[0]);

		//Make this dialog display it.
		//optionPane = new JOptionPane();
		setContentPane(optionPane);
		//add(listScrollPane, BorderLayout.CENTER);
		//add(listScrollPane2, BorderLayout.CENTER);
		

		//Handle window closing correctly.
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				/*
				 * Instead of directly closing the window,
				 * we're going to change the JOptionPane's
				 * value property.
				 */
				optionPane.setValue(new Integer(
						JOptionPane.CLOSED_OPTION));
			}
			public void windowActivated(WindowEvent e) {
             
            }
		});

		//Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter() {
			public void componentShown(ComponentEvent ce) {
			}
		});

		//Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
	}

	/** This method handles events for the text field. */
	public void actionPerformed(ActionEvent e) {
		optionPane.setValue(btnString1);
		
	}

	/** This method reacts to state changes in the option pane. */
	public void propertyChange(PropertyChangeEvent e) {

		String prop = e.getPropertyName();
		
		if (isVisible()
				&& (e.getSource() == optionPane)
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) ||
						JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
			Object value = optionPane.getValue();
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				//ignore reset
				return;
			}

			//Reset the JOptionPane's value.
			//If you don't do this, then if the user
			//presses the same button next time, no
			//property change event will be fired.
			optionPane.setValue(
					JOptionPane.UNINITIALIZED_VALUE);

			if (btnString1.equals(value)) {
				    clearAndHide();
					typedText = null;
				
			} else { //user closed dialog or clicked cancel
				list = null;
				clearAndHide();
			}
		}
	}
	

	
	//This method is required by ListSelectionListener.
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {
 
        
        }
    }
    
    public String getInputValue(String name) {
    	for(Object o:array)
    		if(o instanceof JTextField) {
    			JTextField t = (JTextField)o;
    			if(t.getName().equals(name))
    				return t.getText();
    		}
    	return null;
    }

	/** This method clears the dialog and hides it. */
	public void clearAndHide() {
		setVisible(false);
	}
	
	public JOptionPane getOptionPane() {
		return optionPane;
	}

	public Map<String, Map<Data, String>> getInputs() {
		
		if(list == null)
			return null;
		
		Map<String, Map<Data, String>> cwsinputs = new HashMap<String, Map<Data,String>>();

		int i=0;
		for(DataMessage dm:list) {
			Map<Data, String> message = new HashMap<Data, String>();
	       i++;
	       message.put(dm.getData(), ((JTextField)array[i++]).getText());
	       cwsinputs.put(dm.getReceiverService().getName(), message);
		}
		
		return cwsinputs;
	}
}