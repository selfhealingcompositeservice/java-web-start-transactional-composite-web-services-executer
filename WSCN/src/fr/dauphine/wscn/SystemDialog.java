package fr.dauphine.wscn;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener; //property change stuff

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SystemDialog extends JDialog
implements ActionListener, ListSelectionListener,
PropertyChangeListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates the reusable dialog. 
	 * @param g 
	 * @param g */
	public SystemDialog(MainFrame mainFrame) {
		super(mainFrame, true);
		setTitle("System Properties");
		setSize(500, 200);
        setMinimumSize(new Dimension(500, 200));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		
		JPanel pan=new JPanel();
		pan.setLayout(new FlowLayout());

		JLabel text = new JLabel();
		
		String t = "Number of processors available to the Java virtual machine: ";
		t += Runtime.getRuntime().availableProcessors();
		
		text.setText(t);
        
        pan.add(text);
        add(pan);
        pack();
    }


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
	
		
	}

}