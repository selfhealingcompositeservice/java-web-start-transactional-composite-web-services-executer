package fr.dauphine.wscn;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


/* Used by InternalFrameDemo.java. */
public class SimulationDialog extends JDialog implements ActionListener, ListSelectionListener,
PropertyChangeListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    static final private int height = 350;

    MainFrame mainFrame;
	
	private String btnString1 = "Enter";
	private String btnString2 = "Cancel";
	
	private JOptionPane optionPane;
	
	private JCheckBox simAvailabilityCheck;
	
    
    public SimulationDialog(MainFrame mainFrame) {
        super(mainFrame, true);
        this.mainFrame = mainFrame;
        setTitle("Simulation Properties");
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(200, height);
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        
        simAvailabilityCheck = new JCheckBox("Simulate availability.");  
        
        Object[] array = {simAvailabilityCheck};

        
        Object[] options = {btnString1, btnString2};

		//Create the JOptionPane.
		optionPane = new JOptionPane(array,
				JOptionPane.PLAIN_MESSAGE,
				JOptionPane.YES_NO_OPTION,
				null,
				options,
				options[0]);

		//Make this dialog display it.
		//optionPane = new JOptionPane();
		setContentPane(optionPane);
		
		//Handle window closing correctly.
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				/*
				 * Instead of directly closing the window,
				 * we're going to change the JOptionPane's
				 * value property.
				 */
				optionPane.setValue(new Integer(
						JOptionPane.CLOSED_OPTION));
				setVisible(false);
				
			}
		});

		//Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
        pack();
        
    }


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		
		if (btnString1.equals(optionPane.getValue())) {
				optionPane.setValue(
						JOptionPane.UNINITIALIZED_VALUE);

				setVisible(false);
				mainFrame.getSimulationProperties().setSimulateAvailability(
						simAvailabilityCheck.isSelected());
			
		} else
			setVisible(false);
	}


	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {

	}
    
}
