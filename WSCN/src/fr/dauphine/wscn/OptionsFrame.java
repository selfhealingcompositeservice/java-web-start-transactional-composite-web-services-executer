package fr.dauphine.wscn;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JButton;
import javax.swing.JInternalFrame;

import fr.dauphine.graphutils.GraphUtils;

public class OptionsFrame extends JInternalFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    
    protected JButton addService;
    protected JButton addControlNodes;
    protected JButton removeControlNodes;
    protected JButton executeCWS;
    protected JButton resetCWS;
    JInternalFrame frame;
    
    static AddNodeDialog addNodeDialog;
    

    MainFrame mainFrame;
    
    final Lock lock = new ReentrantLock();
	final Condition notFull  = lock.newCondition();
    
    public OptionsFrame(final MainFrame mainFrame) {
        super("Options", 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable
        this.mainFrame = mainFrame;
        
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(200,300);
        setLayout(new GridLayout(5, 1));
        //Set the window's location.
        //setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
        setLocation(new Double(mainFrame.getCwsFrame().getLocation().getX()).intValue() + mainFrame.getCwsFrame().getSize().width + 200, 
        		new Double(mainFrame.getCwsFrame().getLocation().getY()).intValue());
        frame = this;
        addNodeDialog = new AddNodeDialog(mainFrame);
        addNodeDialog.pack();

        
        addService = new JButton("Add Service");
        addService.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
        		addNodeDialog.setLocationRelativeTo(frame);
        		addNodeDialog.setVisible(true);
            }
        });
        getContentPane().add(addService);
        
        executeCWS = new JButton("Execute CWS");
        executeCWS.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
        		mainFrame.execute();
            }

        });
        getContentPane().add(executeCWS);
        
        addControlNodes = new JButton("Add Control Nodes");
        addControlNodes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
            	GraphUtils.addControlNodes(mainFrame.getGraph());
            }
        });
        getContentPane().add(addControlNodes);
        
        removeControlNodes = new JButton("Remove Control Nodes");
        removeControlNodes.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent ae) {
            	GraphUtils.removeControlNodes(mainFrame.getGraph());
            }
        });
        getContentPane().add(removeControlNodes);
        
        resetCWS = new JButton("Reset CWS");
        resetCWS.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent ae) {
        		mainFrame.getCwsFrame().clearGraph();
            }
        });
        getContentPane().add(resetCWS);
        
    }
    
}
