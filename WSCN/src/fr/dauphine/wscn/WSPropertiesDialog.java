package fr.dauphine.wscn;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.message.DataMessage;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;
import fr.dauphine.service.TransactionalProperty;


/* Used by InternalFrameDemo.java. */
public class WSPropertiesDialog extends JDialog implements ActionListener, ListSelectionListener,
PropertyChangeListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    static final private int height = 350;

    MainFrame mainFrame;

    private JTextField inputsValue;
    private JTextField outputsValue;
    private JTextField priceValue;
	private JTextField executionTimeValue;
	private JTextField reputationValue;
	private JTextField availabilityValue;
	private JTextField transactionalProperty;
	
	private List<DataMessage> inputLists;
	private List<DataMessage> outputLists;
	
	private String btnString1 = "Enter";
	private String btnString2 = "Cancel";
	
	private JOptionPane optionPane;
	
	private Service service;
	
    
    public WSPropertiesDialog(MainFrame mainFrame, Service s) {
        super(mainFrame, true);
        this.mainFrame = mainFrame;
        setTitle("WS Properties");
        
        this.service = s;
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(200, height);
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        
        //getContentPane().add(new JLabel("Name: "));
        JTextField name = new JTextField(s.getName());
        name.setEnabled(false);
        //getContentPane().add(name);
        
        //getContentPane().add(new JLabel("Inputs: "));
        inputsValue = new JTextField(s.getInputs().toString());
        inputsValue.setEnabled(false);
        //getContentPane().add(inputsValue);
        
        //getContentPane().add(new JLabel("Outputs: "));
        outputsValue = new JTextField(s.getOutputs().toString());
        outputsValue.setEnabled(false);
        //getContentPane().add(outputsValue);
        
        //getContentPane().add(new JLabel("Price: "));
        priceValue = new JTextField(new Double(s.getPrice()).toString());
        priceValue.setEnabled(false);
        //getContentPane().add(priceValue);
        
        //reputation
        //getContentPane().add(new JLabel("Reputation: "));
        //getContentPane().add(Box.createHorizontalGlue());
        reputationValue = new JTextField("N/A");
        reputationValue.setEnabled(false);
        //getContentPane().add(reputationValue);
        
        //availability
        //getContentPane().add(new JLabel("Availability: "));
        //getContentPane().add(Box.createHorizontalGlue());
        availabilityValue = new JTextField(new Double(s.getAvailability()).toString());
        availabilityValue.setEnabled(false);
        //getContentPane().add(availabilityValue);
        
        //execution time
        //getContentPane().add(new JLabel("Execution Time: "));
        //getContentPane().add(Box.createHorizontalGlue());
        executionTimeValue = new JTextField(new Double(s.getEstimatedExecutionTime()).toString());
        executionTimeValue.setEnabled(false);
        //getContentPane().add(executionTimeValue);
        
        //transactional property
        //getContentPane().add(new JLabel("Transactional Property: "));
        //getContentPane().add(Box.createHorizontalGlue());
        transactionalProperty = new JTextField(s.getTransactionalProperty().toString());
        transactionalProperty.setEnabled(false);
        //getContentPane().add(transactionalProperty);
        
        
        Object[] array = {new JLabel("Name: "), name, 
        		new JLabel("Inputs: "), inputsValue, 
        		new JLabel("Outputs: "), outputsValue,
        		new JLabel("Price: "), priceValue,
        		new JLabel("Reputation: "), reputationValue,
        		new JLabel("Availability: "), availabilityValue,
        		new JLabel("Execution Time: "), executionTimeValue,
        		new JLabel("Transactional Property: "), transactionalProperty};

        
        Object[] options = {btnString1, btnString2};

		//Create the JOptionPane.
		optionPane = new JOptionPane(array,
				JOptionPane.PLAIN_MESSAGE,
				JOptionPane.YES_NO_OPTION,
				null,
				options,
				options[0]);

		//Make this dialog display it.
		//optionPane = new JOptionPane();
		setContentPane(optionPane);
		
		//Handle window closing correctly.
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				/*
				 * Instead of directly closing the window,
				 * we're going to change the JOptionPane's
				 * value property.
				 */
				optionPane.setValue(new Integer(
						JOptionPane.CLOSED_OPTION));
			}
		});

		//Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
        pack();
        
    }


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		
		if (btnString1.equals(optionPane.getValue())) {
				setVisible(false);
				if(mainFrame.getSimulationProperties().isSimulateAvailability()) {
					Service serviceSim;
					if(mainFrame.getSimulationProperties().getService(service.getName()) == null) {
						serviceSim = new Service(service.getName());
					} else {
						serviceSim = mainFrame.getSimulationProperties().getService(service.getName());
					}
					serviceSim.setAvailability(Double.parseDouble(availabilityValue.getText()));
					mainFrame.getSimulationProperties().addService(serviceSim);
						
				}
	
			
		} else
			setVisible(false);
	}


	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {

	}


	@Override
	public void setVisible(boolean b) {

		availabilityValue.setEnabled(mainFrame.getSimulationProperties().isSimulateAvailability());
		
		super.setVisible(b);
	}
	
	
    
}
