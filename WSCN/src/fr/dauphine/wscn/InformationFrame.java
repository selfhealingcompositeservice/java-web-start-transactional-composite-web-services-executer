package fr.dauphine.wscn;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.message.DataMessage;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Service;
import fr.dauphine.service.TransactionalProperty;


/* Used by InternalFrameDemo.java. */
public class InformationFrame extends JInternalFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    static final private int height = 350;

    MainFrame mainFrame;

    private JTextField inputsValue;
    private JTextField outputsValue;
    private JTextField priceValue;
	private JTextField executionTimeValue;
	private JTextField reputationValue;
	private JTextField availabilityValue;
	private JTextField transactionalProperty;
	
	private List<DataMessage> inputLists;
	private List<DataMessage> outputLists;
	
    
    public InformationFrame(MainFrame mainFrame) {
        super("CWS Information", 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable
        this.mainFrame = mainFrame;
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(200, height);
        
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        getContentPane().add(new JLabel("Inputs: "));
        inputsValue = new JTextField("N/A");
        inputsValue.setEnabled(false);
        getContentPane().add(inputsValue);
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        getContentPane().add(new JLabel("Outputs: "));
        outputsValue = new JTextField("N/A");
        outputsValue.setEnabled(false);
        getContentPane().add(outputsValue);
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        getContentPane().add(new JLabel("Price: "));
        priceValue = new JTextField("N/A");
        priceValue.setEnabled(false);
        getContentPane().add(priceValue);
        
        //reputation
        getContentPane().add(new JLabel("Reputation: "));
        reputationValue = new JTextField("N/A");
        reputationValue.setEnabled(false);
        getContentPane().add(reputationValue);
        
        //availability
        getContentPane().add(new JLabel("Availability: "));
        availabilityValue = new JTextField("N/A");
        availabilityValue.setEnabled(false);
        getContentPane().add(availabilityValue);
        
        //execution time
        getContentPane().add(new JLabel("Execution Time: "));
        executionTimeValue = new JTextField("N/A");
        executionTimeValue.setEnabled(false);
        getContentPane().add(executionTimeValue);
        
        //transactional property
        getContentPane().add(new JLabel("Transactional Property: "));
        transactionalProperty = new JTextField("N/A");
        transactionalProperty.setEnabled(false);
        getContentPane().add(transactionalProperty);

        //Set the window's location.
        //setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
        setLocation(new Double(mainFrame.getCwsFrame().getLocation().getX()).intValue() + mainFrame.getCwsFrame().getSize().width, 
        		new Double(mainFrame.getCwsFrame().getLocation().getY()).intValue());
        /*priceLabel = new JLabel("Price: ");
        getContentPane().add(priceLabel);
        priceValue = new JLabel("0");
        getContentPane().add(priceValue);*/
        
    }

	public void updateInformation() {
		//existence of initial and final vertices
		boolean existControl = GraphUtils.getVertexByName(mainFrame.getGraph(), Constants.INITIAL_NODE) != null
				&& GraphUtils.getVertexByName(mainFrame.getGraph(), Constants.FINAL_NODE) != null;
		
		//execution time
		GraphAnalyser analyser = new GraphAnalyser(mainFrame.getGraph());
		
		if(!existControl)
			GraphUtils.addControlNodes(mainFrame.getGraph());
		executionTimeValue.setText(new Double(
				analyser.getEstimatedExecutionTime(Constants.INITIAL_NODE, Constants.INITIAL_NODE)).toString()
				);
		if(!existControl)
			GraphUtils.removeControlNodes(mainFrame.getGraph());
		//price
		double price = 0.0;
		double reputation = 100.0;
		double availability = 100.0;
		
		inputLists = new ArrayList<>();
		outputLists = new ArrayList<>();
		for(Service s:mainFrame.getGraph().getVertices()) {
			if(!s.isControl()) {
				price += s.getPrice();
				
				//inputs 

				mainFrame.getExecutionManager().setNotMatchedMessaged(s, "getInputs", inputLists);
				mainFrame.getExecutionManager().setNotMatchedMessaged(s, "getOutputs", outputLists);
		
				
				if(transactionalProperty.getText().equals("N/A")) {
					transactionalProperty.setText(s.getTransactionalProperty().toString());
				} else if(transactionalProperty.getText().equals(TransactionalProperty.CompensableRetriable.toString())) {
					if(!s.getTransactionalProperty().equals(TransactionalProperty.CompensableRetriable)) {
						transactionalProperty.setText(s.getTransactionalProperty().toString());
					}
				}
			}
		}
		priceValue.setText(new Double(price).toString());
		availabilityValue.setText(new Double(availability).toString());
		reputationValue.setText(new Double(reputation).toString());
		inputsValue.setText(inputLists.toString());
		outputsValue.setText(outputLists.toString());
		
	}
	
	public List<DataMessage> getInputLists() {
		return inputLists;
	}
	
	static public int getHeightValue() {
		return height;
	}
}
