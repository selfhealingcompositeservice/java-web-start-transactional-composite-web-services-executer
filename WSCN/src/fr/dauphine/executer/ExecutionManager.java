package fr.dauphine.executer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import fr.dauphine.enginethread.EngineThread;
import fr.dauphine.enginethread.ExecuterConstants;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.message.DataMessage;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;
import fr.dauphine.wscn.GetInputsDialog;
import fr.dauphine.wscn.MainFrame;

public class ExecutionManager {
	
	private MainFrame main;
	
	private Map<String, EngineThread> engineThreads;
	static GetInputsDialog getInputsDialog;
	
	public ExecutionManager(MainFrame main) {
		this.main = main;
	}

	public void execute() {
		Map<String, Map<Data, String>> cwsinputs = null;
		
    	setEngineThreads(new HashMap<String, EngineThread>());
    	
    	List<Service> servicesToExecute = null;
    	Map<String, Map<Data, String>> inputs = null;
    	
    	 servicesToExecute = new ArrayList<>();
    	 inputs = new HashMap<>();
    	
    	Map<Data,String> message = new HashMap<>();
    	
    	boolean checkpointed = isCheckpointed();
    	
    	if(checkpointed) {
    		main.getLogFrame().publish(new LogRecord(Level.INFO, "Restarting a checkpointed CWS \n"));
    		
    		//Find WSs to execute
    		
    		findServicesToExecute(servicesToExecute, inputs);
    		main.getCheckpointData().clear(); 
    	
    	} else {
	    	//ask for inputs
	        getInputsDialog = new GetInputsDialog(main, main.getInformationFrame().getInputLists());
	        getInputsDialog.pack();
	        getInputsDialog.setLocationRelativeTo(main);
	        getInputsDialog.setVisible(true);
	
	    	
	        cwsinputs = getInputsDialog.getInputs();
	    	//getInputsDialog.
	    	
	    	if(cwsinputs == null)
	    		return;
	    	
	    	servicesToExecute = new ArrayList<>(main.getGraph().getVertices());
    	}
    	
    	//initialize Engine Threads
    	for(Service s:servicesToExecute) {
			if(!s.isInitial()) {
				EngineThread et = new EngineThread(main, s);
				et.start();
				getEngineThreads().put(s.getName(), et);
			} 
		}
    	
    	//start
    	if(!checkpointed) {
	    	Service initial = GraphUtils.getVertexByName(main.getGraph(), Constants.INITIAL_NODE);
	    	
	    	for(Service succ:main.getGraph().getSuccessors(initial)) {
	    		getEngineThreads().get(succ.getName()).addInputs(cwsinputs.get(succ.getName()));
	    	}
    	} else {
    		
    		for(String name:inputs.keySet())
    			for(Data d:inputs.get(name).keySet()) {
    				message.put(d, inputs.get(name).get(d));
    				getEngineThreads().get(name).addInputs(message);
    				message.clear();
    			}
    	}
    	
    }

	public Map<String, EngineThread> getEngineThreads() {
		return engineThreads;
	}

	public void setEngineThreads(Map<String, EngineThread> engineThreads) {
		this.engineThreads = engineThreads;
	}
	
	 private void findServicesToExecute(List<Service> servicesToExecute, Map<String, Map<Data, String>> inputs) {
    	 
	    	for(Service s:main.getGraph().getVertices()) 
	    		if((s.isInitialState() || s.isAbandoned()) && !s.isInitial()) {
	    			servicesToExecute.add(s);
	    			if(s.isAbandoned())
	    				inputs.put(s.getName(), main.getCheckpointData().getInputs(s));
	    		}
	 }
	 
	 public boolean isCheckpointed() {
		 boolean checkpointed = false;

		 Map<Data,String> fData = main.getCheckpointData().getInputs(GraphUtils.getVertexByName(main.getGraph(), Constants.FINAL_NODE));
		 if(fData != null && fData.containsValue(ExecuterConstants.Messages.CHECKPOINT_TOKEN.toString()))
			 checkpointed = true;

		 return checkpointed;
	 }
	 
	 @SuppressWarnings("unchecked")
	public void setNotMatchedMessaged(Service s, String methodName, List<DataMessage> dataList) {

		 try {

			 Method method = Service.class.getMethod(methodName, (Class[]) null);

			 Method neighborsMethod = (methodName.equals("getInputs")) ? 
					 Service.class.getMethod("getOutputs", (Class[]) null):
						 Service.class.getMethod("getInputs", (Class[]) null);


					 Method getNeighnorsMethod = (methodName.equals("getInputs")) ? 
							 edu.uci.ics.jung.graph.Graph.class.getMethod("getPredecessors", Object.class ):
								 edu.uci.ics.jung.graph.Graph.class.getMethod("getSuccessors",  Object.class);

							 Collection<Service> neighbors = (Collection<Service>) getNeighnorsMethod.invoke(main.getGraph(), s);

							 if(neighbors.size() == 0) {

								 for(Data d:(List<Data>)method.invoke(s, (Object[]) null))
									 dataList.add(
											 new DataMessage(null, s, d, null)
											 );
							 } else {
								 //check if all its inputs are matched with
								 //an output of its predecessors
								 for(Data d:(List<Data>)method.invoke(s, (Object[]) null)) {
									 boolean matched = false;
									 for(Service succ:neighbors) {
										 for(Data d2:(List<Data>)neighborsMethod.invoke(succ, (Object[]) null)) {
											 if(d2.equals(d)) {
												 matched = true;
												 break;
											 }
										 }
									 }
									 if(!matched)
										 dataList.add(
												 new DataMessage(null, s, d, null)
												 );
								 }
							 }
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
	 }
}
