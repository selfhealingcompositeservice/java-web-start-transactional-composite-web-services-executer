package fr.dauphine.file;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Hypergraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.io.GraphIOException;
import edu.uci.ics.jung.io.GraphMLWriter;
import edu.uci.ics.jung.io.graphml.EdgeMetadata;
import edu.uci.ics.jung.io.graphml.GraphMLReader2;
import edu.uci.ics.jung.io.graphml.GraphMetadata;
import edu.uci.ics.jung.io.graphml.HyperEdgeMetadata;
import edu.uci.ics.jung.io.graphml.NodeMetadata;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Service;
import fr.dauphine.ws.Registry;
import fr.dauphine.wscn.MainFrame;

public class FileGraph {
	
	public static void saveGraph(Graph<Service,Number> graph, String filename, final MainFrame mainFrame) {

		try {
			GraphMLWriter<Service, Number> graphWriter = new GraphMLWriter<Service, Number> ();
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename)));
			
			/*graphWriter.addVertexData("x", null, "0",
					new Transformer<Service, Number>() {
				public Number transform(Service v) {
					return Double.toString(graphy.layout.getX(v));
				}
			}
					);

			graphWriter.addVertexData("y", null, "0",
					new Transformer<Service, Number>() {
				public Number transform(Service v) {
					return Double.toString(graphy.layout.getY(v));
				}
			}
					);*/
			graphWriter.addGraphData("ckp", "denotes if the graph was checkpointed", "false", new Transformer<Hypergraph<Service,Number>,String>() {

				@Override
				public String transform(Hypergraph<Service, Number> g) {
					return Boolean.toString(mainFrame.getExecutionManager().isCheckpointed());
				}

			
				});
			
			graphWriter.addEdgeData("id", "", "", new Transformer<Number, String>() {

				@Override
				public String transform(Number edge) {
					
					return edge.toString();
				}
			});
			
			//INPUTS AND OUTPUTS
			graphWriter.addVertexData("inputID_1", null, "",
					new Transformer<Service, String>() {

						@Override
						public String transform(Service s) {
							String str = null;

							if(!mainFrame.getExecutionManager().isCheckpointed() || s.isControl())
								str = "";
							else
								str = s.getInputs().get(0).toString();
							
							return str;
						}

			});
			
			graphWriter.addVertexData("input_1", null, "",
					new Transformer<Service, String>() {

						@Override
						public String transform(Service s) {
							String str = null;

							if(!mainFrame.getExecutionManager().isCheckpointed() || s.isControl())
								str = "";
							else 
								str = (String) mainFrame.getExecutionManager().getEngineThreads().get(s.getName()).getInputs().values().toArray()[0];
							
							return str;
						}

			});
			
			graphWriter.addVertexData("outputID_1", null, "",
					new Transformer<Service, String>() {

						@Override
						public String transform(Service s) {
							String str = null;

							if(!mainFrame.getExecutionManager().isCheckpointed() || s.isControl())
								str = "";
							else
								str = s.getOutputs().get(0).toString();
							
							return str;
						}

			});
			
			graphWriter.addVertexData("output_1", null, "",
					new Transformer<Service, String>() {

						@Override
						public String transform(Service s) {
							String str = null;

							if(!mainFrame.getExecutionManager().isCheckpointed() || s.isControl())
								str = "";
							else 
								str = (String) mainFrame.getExecutionManager().getEngineThreads().get(s.getName()).getOutputs().values().toArray()[0];
							
							return str;
						}

			});
			
			graphWriter.save(graph, out);
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Graph<Service, Number> loadGraph(String filename, Graph<Service, Number> graph2) {
		
		

        
       
		
		/*Factory<Service> vertexFactory = new Factory<Service>() {

			@Override
			public Service create() {
				
				return null;
			}




		};



		Factory<Number> edgeFactory = new Factory<Number>() {


			int n = 0;


			public Number  create() { 
				return meta; 
				}


		};*/
		
		/* Create the Graph Transformer */
		Transformer<GraphMetadata, Graph<Service, Number>>
		graphTransformer = new Transformer<GraphMetadata,
		                          Graph<Service, Number>>() {
		 
		  public Graph<Service, Number>
		      transform(GraphMetadata metadata) {
		        if (metadata.getEdgeDefault().equals(
		        metadata.getEdgeDefault().DIRECTED)) {
		            return new
		            DirectedSparseGraph<Service, Number>();
		        } else {
		            return new
		            UndirectedSparseGraph<Service, Number>();
		        }
		      }
		};
		
		/* Create the Vertex Transformer */
		Transformer<NodeMetadata, Service> vertexTransformer
		= new Transformer<NodeMetadata, Service>() {
		    public Service transform(NodeMetadata metadata) {
		    	Service s = null;
		    	if(metadata.getId().equals(Constants.INITIAL_NODE) || metadata.getId().equals(Constants.FINAL_NODE))
		    		s = new Service(metadata.getId());
		    	
		    	else
		    		s = Registry.getService(metadata.getId());
		        return s;
		    }
		};
		
		/* Create the Edge Transformer */
		 Transformer<EdgeMetadata, Number> edgeTransformer =
		 new Transformer<EdgeMetadata, Number>() {
		     public Number transform(EdgeMetadata metadata) {
		    	 Number e =  Integer.parseInt(metadata.getProperty("id"));
		         return e;
		     }
		 };

		 /* Create the Hyperedge Transformer */
		 Transformer<HyperEdgeMetadata, Number> hyperEdgeTransformer
		 = new Transformer<HyperEdgeMetadata, Number>() {
		      public Number transform(HyperEdgeMetadata metadata) {
		    	  Number e =  Integer.parseInt(metadata.getProperty("id"));
		          return e;
		      }
		 };
			     		

		Graph<Service, Number> graph = new DirectedSparseMultigraph<Service,Number>();
		try {
			
			/* Create the graphMLReader2 */
			GraphMLReader2<Graph<Service, Number>, Service, Number>
			graphReader = new
			GraphMLReader2<Graph<Service, Number>, Service, Number>
			      (new FileReader(filename), graphTransformer, vertexTransformer,
			       edgeTransformer, hyperEdgeTransformer);
			
			graph = graphReader.readGraph();
			
			
			for(Service s:graph.getVertices())
				graph2.addVertex(s);
			for(Number e:graph.getEdges())
				graph2.addEdge(e, graph.getIncidentVertices(e));
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GraphIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Service f = GraphUtils.getVertexByName(graph, Constants.FINAL_NODE);
		if(f != null)
			GraphUtils.setInputsToNode(graph, f);
		
		
		return graph;

	}

}
