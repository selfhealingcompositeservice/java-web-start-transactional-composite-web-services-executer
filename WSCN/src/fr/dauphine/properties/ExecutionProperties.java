package fr.dauphine.properties;

public class ExecutionProperties {
	
	private boolean checkpointingEnabled;
	private boolean forwardRecoveryEnabled;
	private boolean replicationEnabled;
	
	public ExecutionProperties() {
		setCheckpointingEnabled(false);
		forwardRecoveryEnabled = true;
		replicationEnabled= false;
	}


	public boolean isForwardRecoveryEnabled() {
		return forwardRecoveryEnabled;
	}

	public void setForwardRecoveryEnabled(boolean forwardRecoveryEnabled) {
		this.forwardRecoveryEnabled = forwardRecoveryEnabled;
	}

	public boolean isReplicationEnabled() {
		return replicationEnabled;
	}

	public void setReplicationEnabled(boolean replicationEnabled) {
		this.replicationEnabled = replicationEnabled;
	}


	public boolean isCheckpointingEnabled() {
		return checkpointingEnabled;
	}


	public void setCheckpointingEnabled(boolean checkpointingEnabled) {
		this.checkpointingEnabled = checkpointingEnabled;
	}


	@Override
	public String toString() {
		return "ExecutionProperties [checkpointingEnabled="
				+ checkpointingEnabled + ", forwardRecoveryEnabled="
				+ forwardRecoveryEnabled + ", replicationEnabled="
				+ replicationEnabled + "]";
	}


	
	
	
}
