package fr.dauphine.properties;

import java.util.HashMap;
import java.util.Map;

import fr.dauphine.service.Service;

public class SimulationProperties {
	
	private boolean simulateAvailability;
	private Map<String, Service> services;
	
	public SimulationProperties() {
		simulateAvailability = false;
		services = new HashMap<String, Service>();
	}

	public boolean isSimulateAvailability() {
		return simulateAvailability;
	}

	public void setSimulateAvailability(boolean simulateAvailability) {
		this.simulateAvailability = simulateAvailability;
	}

	public Service getService(String name) {
		return services.get(name);
	}
	
	public void addService(Service s) {
		services.put(s.getName(), s);
	}
	
}
