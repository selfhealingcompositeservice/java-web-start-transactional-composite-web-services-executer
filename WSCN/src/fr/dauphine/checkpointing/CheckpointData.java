package fr.dauphine.checkpointing;

import java.util.HashMap;
import java.util.Map;

import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

public class CheckpointData {
	
	private Map<String, Map<Data, String>> inputs;
	private Map<String, Map<Data, String>> outputs;
	
	
	
	public CheckpointData() {
		super();
		this.inputs = new HashMap<String, Map<Data,String>>();
		this.outputs = new HashMap<String, Map<Data,String>>();
	}

	public Map<Data, String> getInputs(Service service) {
		return inputs.get(service.getName());
	}
	
	public void setInputs(Service service, Map<Data, String> inputs) {
		this.inputs.put(service.getName(), inputs);
	}
	
	public Map<Data, String> getOutputs(Service service) {
		return outputs.get(service.getName());
	}
	
	public void setOutputs(Service service, Map<Data, String> outputs) {
		this.outputs.put(service.getName(), outputs);
	}

	@Override
	public String toString() {
		return "CheckpointData [inputs=" + inputs + ", outputs=" + outputs
				+ "]";
	}

	public void clear() {
		inputs.clear();
		outputs.clear();
	}
	
	

}
