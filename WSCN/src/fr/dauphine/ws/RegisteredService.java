package fr.dauphine.ws;

import java.util.List;

public interface RegisteredService {
	
	public void execute() throws Exception;
	public void setInputs(List<Object> inputs);
	public List<Object> getOutputs();

}