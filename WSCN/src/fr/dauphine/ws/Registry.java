package fr.dauphine.ws;

import java.util.HashMap;
import java.util.Map;

import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

public class Registry {
	
	public static Map<String, Class<? extends RegisteredService>> serviceClasses = 
			new HashMap<String, Class<? extends RegisteredService>>();
	
	static Map<String, Service > services = new HashMap<String, Service>();
	
	static public void init() {
		serviceClasses.put("MSTranslate", MicrosoftTranslateService.class);
		serviceClasses.put("Twitter", TwitterService.class);
		serviceClasses.put("GoogleSearchImage", GoogleImageSearchService.class);
		
		Service s1 = new Service("MSTranslate");
		s1.addInput(new Data("text"));
		s1.addOutput(new Data("text"));
		s1.setTransactionalProperty(fr.dauphine.service.TransactionalProperty.CompensableRetriable);
		services.put(s1.getName(), s1);
		
		Service s2 = new Service("Twitter");
		s2.addInput(new Data("text"));
		s2.addOutput(new Data("success"));
		s2.setTransactionalProperty(fr.dauphine.service.TransactionalProperty.CompensableRetriable);
		services.put(s2.getName(), s2);
		
		Service s3 = new Service("GoogleSearchImage");
		s3.addInput(new Data("text"));
		s3.addOutput(new Data("url"));
		s3.setTransactionalProperty(fr.dauphine.service.TransactionalProperty.CompensableRetriable);
		services.put(s3.getName(), s3);
	}
	
	static public Class<? extends RegisteredService> getServiceClass(String name) {
		return serviceClasses.get(name);
	}
	
	static public Service getService(String name) {
		return services.get(name);
	}

}
