package fr.dauphine.ws;
import java.util.ArrayList;
import java.util.List;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;



public class MicrosoftTranslateService  implements RegisteredService {
	
	static String originalText;
	static String translatedText;
	
	
	public static void translate(String originalText) throws Exception {
		// Set your Windows Azure Marketplace client info - See http://msdn.microsoft.com/en-us/library/hh454950.aspx
		Translate.setClientId("123TranslatorDauphine");
		Translate.setClientSecret("aCBT7naNUXAG3i6+4uytPdQMwbfRHTMn+2aJgsZUV9s=");

		translatedText = Translate.execute(originalText, Language.FRENCH, Language.ENGLISH);
		System.out.println("translated: " + translatedText);
	}

	@Override
	public void execute() throws Exception {
		translate(originalText);
	}

	@Override
	public void setInputs(List<Object> inputs) {
		originalText = (String) inputs.get(0);
		
	}

	@Override
	public List<Object> getOutputs() {
		List<Object> outputs = new ArrayList<>();
		outputs.add(translatedText);
		return outputs;
	}
}