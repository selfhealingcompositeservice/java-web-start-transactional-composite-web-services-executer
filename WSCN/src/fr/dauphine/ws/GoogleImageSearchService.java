package fr.dauphine.ws;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.List;

import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;


public class GoogleImageSearchService implements RegisteredService {

	public static String getImageURL(String text) throws IOException, JSONException {
		URL url = new URL("https://ajax.googleapis.com/ajax/services/search/images?" +
                "v=1.0&q=" + text + "&userip=INSERT-USER-IP");
		URLConnection connection = url.openConnection();
		connection.addRequestProperty("Referer", "");
		
		String line;
		StringBuilder builder = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		while((line = reader.readLine()) != null) {
		builder.append(line);
		}
		
		JSONObject json = new JSONObject(builder.toString());
		
		
	
		System.out.println(((JSONObject)json.get("responseData")).get("results"));
		
		// loop array
		JSONArray msg = (JSONArray) ((JSONObject)json.get("responseData")).get("results");

		return (String) ((JSONObject)msg.get(0)).get("url");
	}

	@Override
	public void execute() {
		System.out.println("Not implemented");
	}

	@Override
	public void setInputs(List<Object> inputs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Object> getOutputs() {
		// TODO Auto-generated method stub
		return null;
	}

}
