package fr.dauphine.message;

import java.io.Serializable;

import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

public class DataMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Service senderService;
	private Service receiverService;
	private Data data;
	private String value;
	
	
	public DataMessage(Service senderService, Service receiverService,
			Data data, String value) {
		super();
		this.senderService = senderService;
		this.receiverService = receiverService;
		this.data = data;
		this.value = value;
	}

	public Service getSenderService() {
		return senderService;
	}
	
	public Service getReceiverService() {
		return receiverService;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		String s;
		
		s = receiverService != null ? receiverService.getName():
			(senderService != null ? senderService.getName():"");
		
		s = s.concat("(" + data + ")");
		
		
		return s;
	}
		
}